-- library declaration
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

-- top_level
entity gen_top is
	Port (
		    -- GPLL_CLK 															-- 8 pins
			 clkin_50		: in    std_logic;							  	-- 1.8V    50 MHz: out also to EPM2210F256
			 --clkintop_p		: in    std_logic;	  	-- LVDS    100 MHz prog osc External Term.
			 --clkintop_p		: in    std_logic_vector(1 downto 0);	  	-- LVDS    100 MHz prog osc External Term.
--			 clkinbot_p		: in    std_logic_vector(1 downto 0);    	-- LVDS    100 MHz prog osc clkinbot_p[0]: out clkinbot_p[1] External Term.
--			 clk_125_p		: in    std_logic;                      	-- LVDS    125 MHz GPLL-req's OCT.

		    -- XCVR-REFCLK													   -- 16 pins req's ALTGXB instatiation
			 -- refclk1_ql0_p	: in    std_logic;								-- Default 100MHz
			 -- refclk2_ql1_p	: in    std_logic;								-- Default 644.53125MHz
			 -- refclk4_ql2_p	: in    std_logic;								-- Default 282.5MHz
			--  refclk5_ql2_p	: in    std_logic;								-- Default 148.5MHz
			 -- refclk0_ql0_p	: in    std_logic;								-- Default 100MHz
			 -- refclk0_qr0_p	: in    std_logic;								-- Default 100MHz
			 -- refclk1_qr0_p	: in    std_logic;								-- Default 156.25MHz
			 --refclk2_qr1_p	: in    std_logic;								-- Default 625MHz
			 -- refclk4_qr2_p	: in    std_logic;								-- Default 100MHz
			 -- refclk5_qr2_p	: in    std_logic);								-- Default 270MHz (DisplayPort)

			 -- Si571 VCXO															-- 2 pins
--			 sdi_clk148_up : out   std_logic;								
--			 sdi_clk148_dn : out   std_logic;

			 -- DDR3 																-- 125pins
--	       ddr3_a			: out   std_logic_vector(13 downto 0);		-- SSTL15    Address
--			 ddr3_ba			: out   std_logic_vector(2 downto 0);		-- SSTL15    Bank Address
--			 ddr3_casn		: out   std_logic;								-- SSTL15    Column Address Strobe
--			 ddr3_clk_n		: out   std_logic;								-- SSTL15    Diff Clock - Neg
--			 ddr3_clk_p		: out   std_logic;								-- SSTL15    Diff Clock - Pos
--			 ddr3_cke		: out   std_logic;								-- SSTL15    Clock Enable
--			 ddr3_csn		: out   std_logic;								-- SSTL15    Chip Select
--			 ddr3_dm			: out   std_logic_vector(8 downto 0);		-- SSTL15    Data Write Mask
--			 ddr3_dq			: in    std_logic_vector(71 downto 0);		-- SSTL15    Data Bus
--			 ddr3_dqs_n		: in    std_logic_vector(8 downto 0);		-- SSTL15    Diff Data Strobe - Neg
--			 ddr3_dqs_p		: in    std_logic_vector(8 downto 0);		-- SSTL15    Diff Data Strobe - Pos
--			 ddr3_odt		: out   std_logic;								-- SSTL15    On-Die Termination Enable
--			 ddr3_rasn		: out   std_logic;								-- SSTL15    Row Address Strobe
--			 ddr3_resetn	: out   std_logic;								-- SSTL15    Reset
--			 ddr3_wen		: out   std_logic;								-- SSTL15    Write Enable
--			 rzqin_1p5		: in    std_logic;								-- OCT Pin in Bank 4A

			 -- QDR2																	-- 66 pins
--	       qdrii_a			: out   std_logic_vector(19 downto 0);		-- HSTL15/18    Address
--			 qdrii_bwsn		: out   std_logic_vector(1 downto 0);		-- HSTL15/18    Byte Write Select
--			 qdrii_cq_n		: in    std_logic;								-- HSTL15/18    Read Data Clock - Neg
--			 qdrii_cq_p		: in    std_logic;								-- HSTL15/18    Read Data Clock - Pos
--			 qdrii_d			: out   std_logic_vector(17 downto 0);		-- HSTL15/18    Write Data
--			 qdrii_doffn	: out   std_logic;								-- HSTL15/18    PLL disable (TR=0)
--			 qdrii_k_n		: out	  std_logic;								-- HSTL15/18    Write Data Clock - Neg
--			 qdrii_k_p		: out	  std_logic;								-- HSTL15/18    Write Data Clock - Pos
--			 qdrii_q			: in    std_logic_vector(17 downto 0);		-- HSTL15/18    Read Data
			 -- qdrii_odt		: out   std_logic;								-- HSTL15/18    On-Die Termination Enable (QDRII Cn)
			 -- qdrii_c_p		: in    std_logic;								-- HSTL15/18    Read Data Valid	(QDRII Cp)
			 -- qdrii_qvld		: in    std_logic;								-- HSTL15/18    Read Data Valid	(QDRII Cp)
--			 qdrii_rpsn		: out   std_logic;								-- HSTL15/18    Read Port Select
--			 qdrii_wpsn		: out   std_logic;								-- HSTL15/18    Write Port Select
--			 rzqin_1p8		: in    std_logic;								-- OCT pin for QDRII/+ and RLDRAM II

			 -- RLDRAM2 															-- 58 pins
--	       rldc_a			: out   std_logic_vector(22 downto 0);		-- HSTL15/18    Address
--			 rldc_ba			: out   std_logic_vector(2 downto 0);		-- HSTL15/18    Bank Address
--			 rldc_ck_n		: out   std_logic;								-- HSTL15/18    Input Clock - Neg
--			 rldc_ck_p		: out   std_logic;								-- HSTL15/18    Input Clock - Pos
--			 rldc_dq			: inout std_logic_vector(17 downto 0);		-- HSTL15/18    Data
--			 rldc_dk_n		: out   std_logic;								-- HSTL15/18    Write (Input) Data Clock - Neg
--			 rldc_dk_p		: out   std_logic;								-- HSTL15/18    Write (Input) Data Clock - Pos
--			 rldc_qk_n		: in    std_logic_vector(1 downto 0);		-- HSTL15/18    Read (Output) Data Clock - Neg
--			 rldc_qk_p		: in 	  std_logic_vector(1 downto 0);		-- HSTL15/18    Read (Output) Data Clock - Pos
--			 rldc_dm			: out	  std_logic;								-- HSTL15/18    Input Data Mask
--			 rldc_qvld		: in 	  std_logic;								-- HSTL15/18    Read Data Valid
--			 rldc_csn		: out	  std_logic;								-- HSTL15/18    Chip Select
--			 rldc_wen		: out	  std_logic;								-- HSTL15/18    Write Enable
--			 rldc_refn		: out	  std_logic;								-- HSTL15/18    Ref Command

			 -- Ethernet   														-- 8 pins
--	       enet_intn		: in    std_logic;								-- 2.5V    MDIO Interrupt (TR=0)
--			 enet_mdc		: out   std_logic;								-- 2.5V    MDIO Clock (TR=0)
--			 enet_mdio		: inout std_logic;								-- 2.5V    MDIO Data (TR=0)
--			 enet_resetn	: out   std_logic;								-- 2.5V    Device Reset (TR=0)
--			 enet_rx_p		: in	  std_logic;								-- LVDS NEED EXTERNAL TERM   SGMII Receive-req's OCT
--			 enet_tx_p		: out   std_logic;								-- LVDS    SGMII Transmit

          -- FSM																	-- 74 pins
	       -- fm_a				: out   std_logic_vector(26 downto 0);		-- 1.8V    Address
--			 fm_d				: in    std_logic_vector(31 downto 0);		-- 1.8V    Data
--			 flash_advn		: out   std_logic;								-- 1.8V    Flash Address Valid
--			 flash_cen		: out   std_logic_vector(1 downto 0);		-- 1.8V    Flash Chip Enable
--			 flash_clk		: out   std_logic;								-- 1.8V    Flash Clock
--			 flash_oen		: out   std_logic;								-- 1.8V    Flash Output Enable
--			 flash_rdybsyn	: in    std_logic_vector(1 downto 0);		-- 1.8V    Flash Ready/Busy
--			 flash_resetn	: out   std_logic;								-- 1.8V    Flash Reset
--			 flash_wen		: out   std_logic;								-- 1.8V    Flash Write Enable
--			 max5_ben		: out   std_logic_vector(3 downto 0);		-- 1.5V    Max V Byte Enable Per Byte
--			 max5_clk		: in    std_logic;								-- 1.5V    Max V Clk
--			 max5_csn		: out   std_logic;								-- 1.5V    Max V Chip Select
--			 max5_oen		: out   std_logic;								-- 1.5V    Max V Output Enable
--			 max5_wen		: out   std_logic;								-- 1.5V    Max V Write Enable		
			 
			 -- Configuration														-- 32 pins
			 -- fpga_data		: out std_logic_vector(31 downto 0);		-- 2.5V    Configuration Data
			 
			 -- Character-LCD														-- 11 pins
			 lcd_csn       : out    std_logic;  							-- 2.5V    LCD Chip Select
			 lcd_d_cn      : out    std_logic; 								-- 2.5V LCD Data / Command Select
			 lcd_data      : inout  std_logic_vector(7 downto 0);	 	-- 2.5V    LCD Data
			 lcd_wen       : out    std_logic; 								-- 2.5V    LCD Write Enable
			 
			 -- User-IO																 -- 27 pins
			 user_dipsw		: in     std_logic_vector(7 downto 0);	    -- HSMB_VAR    User DIP Switches (TR=0)
			 user_led_g    : out    std_logic_vector(7 downto 0);	    -- 2.5V	       User LEDs
			 user_led_r    : out    std_logic_vector(7 downto 0);	    -- 2.5V/1.8V   User LEDs
			 user_pb       : in     std_logic_vector(2 downto 0);	    -- HSMB_VAR    User Pushbuttons (TR=0)
--			 cpu_resetn    : in     std_logic;	 							 -- 2.5V        CPU Reset Pushbutton (TR=0)
			 
			 -- PCI-Express														 -- 25 pins 
			 -- pcie_rx_p     : in     std_logic_vector(7 downto 0);		 -- PCML14    PCIe Receive Data-req's OCT
			 -- pcie_tx_p     : out    std_logic_vector(7 downto 0);		 -- PCML14    PCIe Transmit Data
			 -- pcie_refclk_p : in     std_logic;	 							 -- HCSL      PCIe Clock- Terminate on MB
--			 pcie_led_g3   : out    std_logic;	 							 -- 2.5V      User LED - Labeled Gen3
--			 pcie_led_g2   : out    std_logic;	  							 -- 2.5V      User LED - Labeled Gen2
--			 pcie_led_x1   : out    std_logic;	 							 -- 2.5V      User LED - Labeled x1
--			 pcie_led_x4   : out    std_logic;	  							 -- 2.5V      User LED - Labeled x4
--			 pcie_led_x8   : out    std_logic;	  							 -- 2.5V      User LED - Labeled x8
--			 pcie_perstn   : in     std_logic;	 							 -- 2.5V      PCIe Reset 
--			 pcie_smbclk   : in     std_logic;	 							 -- 2.5V      SMBus Clock (TR=0)
--			 pcie_smbdat   : in     std_logic;	 							 -- 2.5V      SMBus Data (TR=0)
--			 pcie_waken    : out    std_logic;	  							 -- 2.5V      PCIe Wake-Up (TR=0) 
                                                 						 -- must install 0-ohm resistor

			 -- USB 2.0																 -- 19 pins
--			 usb_data		: in     std_logic_vector(7 downto 0);		 -- 1.5V from MAXV
--			 usb_addr		: in     std_logic_vector(1 downto 0);		 -- 1.5V from MAXV
--			 usb_clk			: in     std_logic;	 							 -- 3.3V from Cypress USB
--			 usb_full		: out    std_logic;	 							 -- 1.5V from MAXV
--			 usb_empty		: out    std_logic;	 							 -- 1.5V from MAXV
--			 usb_scl			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_sda			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_oen			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_rdn			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_wrn			: in     std_logic;	 							 -- 1.5V from MAXV
--			 usb_resetn		: in     std_logic;	 							 -- 1.5V from MAXV

			 -- QSFP 																 -- 23 pins
			 -- qsfp_tx_p		: out    std_logic_vector(3 downto 0);
			 -- qsfp_rx_p		: in     std_logic_vector(3 downto 0);
--			 qsfp_mod_seln	: out    std_logic;
--			 qsfp_rstn		: out    std_logic;
--			 qsfp_scl		: out    std_logic;
--			 qsfp_sda		: inout  std_logic;
--			 qsfp_interruptn : in   std_logic;
--			 qsfp_mod_prsn	: in     std_logic;
--			 qsfp_lp_mode	: out    std_logic;
	
			 -- DispayPort x4														 -- 12 pins
			 -- dp_ml_lane_p	: out	   std_logic_vector(3 downto 0);	 -- Transceiver Data
--			 dp_aux_p		: in 	   std_logic;								 -- LVDS (bi-directional) Auxillary Channel
--			 dp_aux_tx_p	: out    std_logic;				 				 -- LVDS (transmit side) Auxillary Channel
			 -- dp_aux_ch_p	: inout 	   std_logic;								 -- LVDS (bi-directional) Auxillary Channel
			 -- dp_aux_ch_n	: inout 	   std_logic;								 -- LVDS (bi-directional) Auxillary Channel
--			 dp_hot_plug	: in 	   std_logic;								 -- 2.5V  Hot Plug Detect
--			 dp_return		: out	   std_logic;								 -- 2.5V  Return for power
--			 dp_direction	: out	   std_logic;								 -- 2.5V  Direction Select on M-LVDS Transceiver

			 -- SDI-Video-Port													 -- 7 pins
			 -- sdi_rx_p		: in 	   std_logic;	          				 -- PCML14   -- SDI Video : in-req's OCT
			 -- sdi_tx_p		: out	   std_logic;	          				 -- PCML14   -- SDI Video : out
			 -- sdi_clk148_dn	: out	   std_logic;	     				 		 -- 2.5V     -- VCO Frequency Down
			 -- sdi_clk148_up	: out	   std_logic;	     						 -- 2.5V     -- VCO Frequency Up
--			 sdi_tx_sd_hdn	: out	   std_logic;	       					 -- 2.5V     -- HD Mode Enable
--			 sdi_tx_en		: out	   std_logic;								 -- 2.5V   -- Transmit Enable
--			 sdi_rx_en		: out	   std_logic;								 -- 2.5V   -- Receive Enable - Tri-state
--			 sdi_rx_bypass	: out	   std_logic;								 -- 2.5V   -- Receive Bypass

			 -- Transceiver-SMA-: 												 -- 2 pins
			 -- sma_tx_p		: out     std_logic;   							 -- PCML14   -- SMA : out Pair

			 -- HSMC-Port-A													 	 -- 107 pins
			 -- hsma_rx_p		: in     std_logic_vector(7 downto 0);	 	 -- PCML14   -- HSMA Receive Data-req's OCT
			 -- hsma_tx_p		: out	   std_logic_vector(7 downto 0);     -- PCML14   -- HSMA Transmit Data
																						 -- Enable below for CMOS HSMC        
			 -- hsma_d			: in 	   std_logic_vector(79 downto 0);	 -- 2.5V     -- HSMA CMOS Data Bus
																						 -- Enable below for LVDS HSMC        
--			 hsma_clk_in0	: in 	   std_logic;	         				 -- 2.5V     -- Primary single-ended CLKIN
--			 hsma_clk_in_p1 :in 		std_logic;	    					    -- LVDS     -- Secondary diff. CLKIN
--			 hsma_clk_in_p2 :in 		std_logic;	      					 -- LVDS     -- Primary Source-Sync CLKIN
--			 hsma_clk_out0	: out	   std_logic;	     					    -- 2.5V     -- Primary single-ended CLKOUT
--			 hsma_clk_out_p1 :out	std_logic;	      					 -- LVDS     -- Secondary diff. CLKOUT
--			 hsma_clk_out_p2 :out	std_logic;	     						 -- LVDS     -- Primary Source-Sync CLKOUT
			 hsma_d			: inout	std_logic_vector(3 downto 0); 	 -- 2.5V     -- Dedicated CMOS IO
--			 hsma_prsntn	: in	   std_logic;	         				 -- 2.5V     -- HSMC Presence Detect : in
			 --hsma_rx_d_p	: in	   std_logic_vector(1 downto 1);	 -- LVDS     -- LVDS Sounce-Sync : in
			 hsma_tx_d_p	: out	   std_logic_vector(1 downto 0)	 -- LVDS     -- LVDS Sounce-Sync : out
--			 hsma_rx_led	: out	   std_logic;	         				 -- 2.5V     -- User LED - Labeled RX
--			 hsma_scl		: out	   std_logic;	            			 -- 2.5V     -- SMBus Clock
--			 hsma_sda		: in	   std_logic;	            			 -- 2.5V     -- SMBus Data
--			 hsma_tx_led	: out	   std_logic;	         				 -- 2.5V     -- User LED - Labeled TX
	

			 -- HSMC-Port-B	 												 	 -- 107pins 
			 -- hsmb_rx_p		: in 	   std_logic_vector(7 downto 0);	    -- PCML14   -- HSMB Receive Data-req's OCT
			 -- hsmb_tx_p		: out	   std_logic_vector(7 downto 0);     -- PCML14   -- HSMB Transmit Data
																						 -- Enable below for CMOS HSMC        
			 -- hsmb_d			: in 	   std_logic_vector(79 downto 0);	 -- 2.5V     -- HSMB CMOS Data Bus
																						 -- Enable below for LVDS HSMC        
--			 hsmb_clk_in0	: in 	   std_logic;	        				    -- 2.5V     -- Primary single-ended CLKIN
--			 hsmb_clk_in_p1 : in  	std_logic;	     						 -- LVDS     -- Secondary diff. CLKIN
--			 hsmb_clk_in_p2 : in 	std_logic;	      					 -- LVDS     -- Primary Source-Sync CLKIN
--			 hsmb_clk_out0	: out	   std_logic;	       					 -- 2.5V     -- Primary single-ended CLKOUT
--			 hsmb_clk_out_p1 : out	std_logic;	     						 -- LVDS     -- Secondary diff. CLKOUT
--			 hsmb_clk_out_p2 : out	std_logic;	    						 -- LVDS     -- Primary Source-Sync CLKOUT
			 -- hsmb_d: inout	    		std_logic_vector(3 downto 0);     -- 2.5V     -- Dedicated CMOS IO
																						 -- DQS Standard - 1.5V/1.8V/2.5V standards  --  -- /
--			 hsmb_a			: inout	std_logic_vector(15 downto 0);	 -- Address 
--			 hsmb_addr_cmd	: inout	std_logic_vector(0 downto 0);		 -- Additional Addres/Command pins
--			 hsmb_ba			: inout	std_logic_vector(3 downto 0);		 -- Bank Address
--			 hsmb_casn		: inout	std_logic;					  
--			 hsmb_rasn		: inout	std_logic;								  
--			 hsmb_wen		: inout	std_logic;								  
--			 hsmb_cke		: inout	std_logic;					 			 -- Clock Enable
--			 hsmb_csn		: inout	std_logic;								 -- Chip Select
			 -- hsmb_c_p		: out	   std_logic;								 -- c_p = QVLD; c_n = ODT
--			 hsmb_odt		: inout	std_logic;								 -- ODT
--			 hsmb_qvld		: inout	std_logic;								 -- QVLD
--			 hsmb_dm			: inout	std_logic_vector(3 downto 0);		 -- Data Mask
--			 hsmb_dq			: inout	std_logic_vector(31 downto 0);	 -- Data
--			 hsmb_dqs_p		: inout	std_logic_vector(3 downto 0);		 -- Data Strobe positive
--			 hsmb_dqs_n		: inout	std_logic_vector(3 downto 0);		 -- Data Strobe negative
	
--			 hsmb_prsntn	: in 	   std_logic;	         				 -- 2.5V     -- HSMC Presence Detect : in
--			 hsmb_rx_led	: out	   std_logic;	         				 -- 2.5V     -- User LED - Labeled RX
--			 hsmb_scl		: out	   std_logic;	            			 -- 2.5V     -- SMBus Clock
--			 hsmb_sda		: in	   std_logic;	            			 -- 2.5V     -- SMBus Data
--			 hsmb_tx_led	: out 	std_logic;	         			    -- 2.5V     -- User LED - Labeled TX
	
--			 rzqin_hsmb_var : in 	std_logic
														);	
end gen_top;

architecture gen_top_arc of gen_top is

	component PLL_ts 
		port (
			refclk   		: in  std_logic; 												-- OPT  refclk.clk (50MHz)
			rst      		: in  std_logic; 												-- OPT  reset.reset
			outclk_0 		: out std_logic;        									-- outclk0.clk
			outclk_1 		: out std_logic;        									-- outclk1.clk
			outclk_2 		: out std_logic;        									-- outclk2.clk
			locked   		: out std_logic);         									--  locked.export
	end component;

	component gray_counter 
		Port (
			clk		   	: in  std_logic;
		   reset	   		: in  std_logic;
			enable	   	: in  std_logic;
			gray_count 		: out std_logic_vector(7 downto 0));
	end component;
	
	component rnGen 
		Port (
			clk 				: in	std_logic;
			rst				: in  std_logic;
			en					: in  std_logic;
			rn					: out std_logic_vector(7 downto 0));
	end component;
	
	component outputManager is
		port(
			clk 	 			: in  std_logic;
			rst 	 			: in  std_logic;
			row 	 			: in  std_logic_vector(7 downto 0);
			col 	 			: in  std_logic_vector(7 downto 0);
			tstamp 			: in  std_logic_vector(7 downto 0);
			set_rn		 	: in  std_logic;
			set_tstamp_rn 	: in  std_logic;		
			user_set_datak : in  std_logic;
			data				: out std_logic_vector(7 downto 0);
			set_datak 		: out std_logic;
			send_event		: out std_logic);
	end component;
	
	component enc_8b10b
     port(
         RESET 			: in std_logic ;        								-- Global asynchronous reset (active high)
         SBYTECLK 		: in std_logic ;    										-- Master synchronous send byte clock
         KI 				: in std_logic ; 											-- Control (K) input(active high)
         AI, BI, CI, DI, EI, FI, GI, HI 			: in std_logic ;    		-- Unencoded input data
         JO, HO, GO, FO, IO, EO, DO, CO, BO, AO : out std_logic);  			-- Encoded out
	end component;
	
	component lvds_tx
		Port (
			pll_areset		: in	std_logic;
			tx_in				: in  std_logic_vector(9 downto 0);
			tx_inclock		: in	std_logic;
			tx_locked		: out	std_logic;
			tx_out			: out std_logic_vector(0 downto 0);
			tx_outclock		: out std_logic);
	end component;

--LCD Part
	component controllerLine
			port(
					line1 : in std_logic_vector(63 downto 0) := (others=>'0');
					line2 : in std_logic_vector(63 downto 0) := (others=>'0');
	
					clock: in std_logic;											-- Reference Clock
					reset: in std_logic;											-- Manual reset_n
					enable_in: in std_logic;									-- external LCD enable
					pause_lcd: in std_logic;									-- '1': lcd does not update data, '0': continous update of incoming data
					
					lcd_enable: out std_logic;									-- On/Off Switch, connect to lcd_csn, 1 is on, 0 is off
					lcd_rs: out std_logic;										-- Data/Setup Switch, connect to lcd_d_cn, 1 is data, 0 is setup
					lcd_rw: out std_logic;										-- Read/Write Switch, connect to lcd_wen, 1 is read, 0 is write
					busy: out std_logic;											-- Busy Feedback
					lcd_data: out std_logic_vector (7 downto 0);			-- LCD Data
					ready_out: out std_logic									-- ready signal
					
		);	
	end component;
	

	
	
	signal user_rst, user_set_datak, user_en_gen, user_set_rn, user_set_tstamp_rn  : std_logic;
	signal stat_pll_locked, stat_tx_locked, stat_send_event : std_logic;
	signal set_datak : std_logic;
	
	
	-- clocks
	signal clk_frame, clk_hits, clk_words, clk_tx_out : std_logic;
	
	-- data
	signal tstamp, row, col : std_logic_vector (7 downto 0) := (others=>'0');
	signal data_8b				: std_logic_vector (7 downto 0) := (others=>'0');
	signal data_10b			: std_logic_vector (9 downto 0) := (others=>'0');
	-- signals
	signal tx_signal : std_logic;
	
	-- LCD
	signal	lcd_busy, lcd_ready	: std_logic;
--	signal lcd_line2 : std_logic_vector(63 downto 0) :=(others=>'1');
--	signal lcd_line1 : std_logic_vector(63 downto 0) :=(others=>'1');
--	signal lcd_wait : integer := 0;
	

	
begin				
	globalPLL : PLL_ts port map (
		refclk   		=> clkin_50,
		rst      		=> user_rst,
		outclk_0 		=> clk_frame,
		outclk_1 		=> clk_hits,
		outclk_2			=> clk_words,
		locked   		=> stat_pll_locked);
		
	timestamp: gray_counter port map(	
		clk	 			=> clk_frame,
		reset				=> user_rst,
		enable			=> user_en_gen,
		gray_count		=>	tstamp);
	
	rowGen : rnGen port map(
		clk 				=> clk_words,
		rst				=> user_rst,
		en					=> user_en_gen,
		rn					=> row);
		
	colGen : rnGen port map(
		clk				=> clk_words,
		rst				=> user_rst,
		en					=> user_en_gen,
		rn					=> col);

	outMan : outputManager port map(
		clk 	 			=> clk_words,
		rst 	 			=> user_rst,
		row 	 			=> row,
		col 	 			=> col,
		tstamp 			=> tstamp,
		--tstamp			=> row,
		set_rn		 	=> user_set_rn,
		set_tstamp_rn	=> user_set_tstamp_rn,
		data				=> data_8b,
		user_set_datak => user_set_datak,
		set_datak 		=> set_datak,
		send_event		=> stat_send_event);
		
	encoder : enc_8b10b port map(
         RESET 		=> user_rst,
         --SBYTECLK 	=> clkin_50,
			SBYTECLK 	=> clk_words,
         KI 			=> set_datak,
         AI 			=> data_8b(0),
			BI 			=> data_8b(1),
			CI				=> data_8b(2),
			DI				=> data_8b(3),
			EI				=> data_8b(4),
			FI				=> data_8b(5),
			GI				=> data_8b(6),
			HI 			=> data_8b(7),
         JO				=> data_10b(9),
			HO				=> data_10b(8),
			GO				=> data_10b(7),
			FO				=> data_10b(6),
			IO				=> data_10b(5),
			EO				=> data_10b(4),
			DO				=> data_10b(3),
			CO				=> data_10b(2),
			BO				=> data_10b(1),
			AO 			=> data_10b(0));
		
	transiver : lvds_tx port map(
		pll_areset		=> user_rst,
		tx_in				=> data_10b,
		-- tx_in				=> "0110000011",			-- debugin datak, 3C
		-- tx_in				=> "0101010101",				-- debugin
		tx_inclock		=> clkin_50,
		tx_locked		=> stat_tx_locked,
		tx_out(0)		=> tx_signal,
		tx_outclock		=> clk_tx_out);

	
	-- -- -- -- -- OUTPUT -- -- -- -- -- 
--	lcd_buffer : process(clkin_50) begin
--		if(rising_edge(clkin_50)) then
--			if(lcd_wait > 5000000) then
--				lcd_line2 <= x"00000000000000" & data;
--				lcd_wait <= 0;
--			else
--				lcd_wait <= lcd_wait + 1;
--			end if;
--		end if;
--	end process lcd_buffer;
	
	LCD: controllerLine port map(	line1					 	=> "000" & data_10b(9) & "000" & data_10b(8) & "000" & data_10b(7) & "000" & data_10b(6) & "000" & data_10b(5) & "000" & data_10b(4) & "000" & data_10b(3) & "000" & data_10b(2) & "000" & data_10b(1) & "000" & data_10b(0) & x"0000" & tstamp,
											line2						=> data_8b & x"00000000" & col & x"00"& row,
											clock						=> clkin_50,											-- Reference Clock
											reset						=> user_rst,												-- Manual reset_n
											enable_in				=> '1',														-- external LCD enable
											pause_lcd				=> user_dipsw(7),											-- '1': lcd does not update data, '0': continous update of incoming data
											
											lcd_enable				=> lcd_csn,													-- On/Off Switch, connect to lcd_csn, 1 is on, 0 is off
											lcd_rs					=> lcd_d_cn,													-- Data/Setup Switch, connect to lcd_d_cn, 1 is data, 0 is setup
											lcd_rw					=> lcd_wen,													-- Read/Write Switch, connect to lcd_wen, 1 is read, 0 is write
											busy						=> lcd_busy,												-- Busy Feedback
											lcd_data(7 downto 0)	=> lcd_data(7 downto 0),												-- LCD Data
											ready_out				=> lcd_ready);												-- ready signal)
					

					
	-- input assignments
	user_rst				<= not user_pb(0);
	
	
	user_en_gen		 	<= user_dipsw(0);
	user_set_rn			<= user_dipsw(1);
	user_set_tstamp_rn<= user_dipsw(2);
	user_set_datak		<= user_dipsw(3);
		
	-- outputs assignments
	user_led_r(0)	<= not lcd_busy;
	user_led_g(0)	<= not lcd_ready;
	user_led_g(1)	<= not stat_pll_locked;
	user_led_g(2)	<= not stat_tx_locked;
	user_led_g(3)	<= not (stat_pll_locked and stat_tx_locked);
	user_led_g(4)	<= not stat_send_event;
	user_led_g(7 downto 5) <= (others => '1');
	user_led_r(7 downto 1) <= (others => '1');
	
	-- tx signal
	hsma_tx_d_p(0) 	<= tx_signal;
	hsma_d(0)			<= clk_words;
	hsma_d(1)			<= clkin_50;
	hsma_d(3)			<= clk_hits;
	hsma_tx_d_p(1)		<= clk_tx_out;
	
end gen_top_arc;
