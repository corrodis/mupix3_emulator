-- generates timestamp
-- gray counter, 50ns frames (50MHz clk)
-- use 50MHz/100Mhz input clk to run at 50ns/25ns frame size


-- library declaration

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity timestamp is
	port( clk		: in std_logic;
			rst		: in std_logic;
			tstamp	: out std_logic_vector (7 downto 0));
end timestamp;

architecture timestamp_arc of timestamp is

	component gray_counter 
		Port (clk		   : in std_logic;
			   reset	   	: in std_logic;
			   enable	   : in std_logic;
			   gray_count 	: out std_logic_vector(7 downto 0));
	end component;
	
	component PLL_ts
		port (refclk   	: in  std_logic; 									--  refclk.clk
				rst      	: in  std_logic; 									--   reset.reset
				outclk_0 	: out std_logic);         						-- outclk0.clk
	end component;

	signal clk_in : std_logic;	

begin

	PLL: PLL_ts port map(
		refclk   => clk,
		rst      => rst,
		outclk_0 => clk_in);

	counter: gray_counter port map(	
		clk	 		=> clk_in,
		reset			=> rst,
		enable		=> '1',
		gray_count	=>	tstamp);

end timestamp_arc;
			
			