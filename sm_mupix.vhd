-- State Machine to manage input from read out FPGA (telescope) and to generate dummy data
-- 21/11/2013		simple version, always send 5x4 events, no tstamp disorder
-- 03/12/2013		version with state machine, input comes from extern
-- Simon Corrodi

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity sm_mupix is
	Port(
		CLK			: in  std_logic;
		RST			: in  std_logic;
		LDPIX			: in  std_logic;
		LDCOL			: in  std_logic;
		RDCOL			: in  std_logic;
		PULLDOWN		: in  std_logic;
		COL			: in  std_logic_vector(5 downto 0);
		ROW			: in  std_logic_vector(5 downto 0);
		TSTAMP		: in  std_logic_vector(7 downto 0);
		SET_PIX_NO	: in  std_logic;
	
		BUSY			: out std_logic;
		DATA_INV		: out std_logic_vector(31 downto 0);
		PRIOUT		: out std_logic;
		
		STAT_STATE 	: out std_logic_vector(3 downto 0);
		READOUT_NO	: out integer;
		EVENTS_NO	: out integer
	);
end entity;

architecture RTL of sm_mupix is

	-- settings
	constant colsNo : integer := 5;
	constant rowsNo : integer := 5;

	type state_type is (s_wait, s_data_available, s_ldcol_data, s_ldcol_empty);
	signal state : state_type;
	
	-- counter
	signal cnt_cols, cnt_rows 				: integer range 0 to 64;
	signal cnt_readouts, cnt_events 		: integer;
	
	-- buffer signals to detect risign edges
	signal LDCOL_last, LDPIX_last, RDCOL_last : std_logic;
	
begin

	sm : process(CLK) begin
		if(RST = '0') then
			-- reset 
			BUSY				<= '0';
			PRIOUT			<= '0';
			-- counter
			cnt_rows			<= 0;
			cnt_cols			<= 0;
			cnt_readouts	<= 0;
			cnt_events		<= 0;
			-- reset state
			state 		<= s_wait;
		elsif(rising_edge(CLK)) then
			case state is
				-- -- -- STATE: WAIT dont do anything, just wait for new readout -- -- --
				when s_wait =>
					-- outputs
						BUSY			<= '0';
						PRIOUT		<= '0';
						
					-- transitions to other states
						-- load pixel into memory
						if(LDPIX = '1' and LDPIX_last = '0') then
							-- load pixels into buffer
							-- what ever is needed for emulator
							state <= s_data_available;
							-- set counters of current readout cycle to zero
							cnt_rows			<= 0;
							cnt_cols			<= 0;
							cnt_readouts 	<= cnt_readouts + 1;
						else
							state <= s_wait;
						end if;
				
				-- -- -- STATE: DATA AVAIABLE now data is loaded -- -- --
				when s_data_available =>	
					-- outputs
						BUSY			<= '1';
					-- transitions to other states
						if(LDCOL = '1' and LDCOL_last = '0') then
							-- load bus
							-- check wether bus has data
							if(cnt_cols+1 < colsNo and rowsNo > 0) then
								state 			<= s_ldcol_data;
								PRIOUT			<= '1';
								if(SET_PIX_NO = '1') then
									DATA_INV			<= x"F0F" & not std_logic_vector(TO_UNSIGNED(cnt_rows,6)) & not std_logic_vector(TO_UNSIGNED(cnt_cols,6)) & not TSTAMP;
								else
									DATA_INV			<= x"F0F" & not ROW & not COL & not TSTAMP;
								end if;
								cnt_rows 		<= cnt_rows + 1;
								cnt_events		<= cnt_events + 1;
							else
								state <= s_ldcol_empty;
								PRIOUT			<= '0';
							end if;
						else
							state <= s_data_available;
						end if; 
				-- -- -- STATE: LDCOL DATA read out data and still some more in the col buffer -- -- --		
				when s_ldcol_data =>	
					-- outputs
					BUSY			<= '1';
					
					-- -- read data if RDCOL -- --
					if(RDCOL = '1' and RDCOL_last = '0') then
						if(SET_PIX_NO = '1') then
							DATA_INV			<= x"F0F" & not std_logic_vector(TO_UNSIGNED(cnt_rows,6)) & not std_logic_vector(TO_UNSIGNED(cnt_cols,6)) & not TSTAMP;
						else
							DATA_INV			<= x"F0F" & not ROW & not COL & not TSTAMP;
						end if;
						cnt_rows 		<= cnt_rows + 1;
						cnt_events		<= cnt_events + 1;
					end if;
						
					-- transitions to other states
						if(cnt_rows < rowsNo) then
							state <= s_ldcol_data;
							PRIOUT		<= '1';
						else
							state <= s_ldcol_empty;
							PRIOUT		<= '0';
							cnt_rows		<= 0;
						end if;
				
			-- -- -- STATE: LDCOL EMPTY col buffer is empty, wait for ldcol to load new data -- -- --				
				when s_ldcol_empty =>	
					-- outputs
					BUSY			<= '1';
						
					-- transitions to other states
						-- check wether still data avaiable
						if(cnt_cols+1 < colsNo) then
							if(LDCOL = '1' and LDCOL_last = '0') then
								-- load new col
								state 			<= s_ldcol_data;
								PRIOUT			<= '1';
								cnt_cols 		<= cnt_cols + 1;
								-- and set first data point?
								if(SET_PIX_NO = '1') then
									DATA_INV			<= x"F0F" & not std_logic_vector(TO_UNSIGNED(cnt_rows,6)) & not std_logic_vector(TO_UNSIGNED(cnt_cols,6)) & not TSTAMP;
								else
									DATA_INV			<= x"F0F" & not ROW & not COL & not TSTAMP;
								end if;
								cnt_rows 		<= cnt_rows + 1;
								cnt_events		<= cnt_events + 1;
							else 
								state 			<= s_ldcol_empty;
								PRIOUT			<= '0';
							end if;
						else
							-- end of data block, go to wait state
							-- dont do anything before new data is loaded by using ldpix
							state 			<= s_wait;
							PRIOUT			<= '0';
						end if;
						
				when others =>
					state <= s_wait;
					
			end case;
			-- buffer the actual signals to ensure that things happen only at rising edges
			LDCOL_last <= LDCOL;
			RDCOL_last <= RDCOL;
			LDPIX_last <= LDPIX;
		end if;
				
	end process;

	with state select
		STAT_STATE <= 	"0001" when s_wait,
						   "0010" when s_data_available,
							"0100" when s_ldcol_data,
							"1000" when s_ldcol_empty;
							

		READOUT_NO	<= cnt_readouts;
		EVENTS_NO	<= cnt_events;

end RTL;