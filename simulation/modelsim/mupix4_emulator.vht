-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2013 11:05:19"
                                                            
-- Vhdl Test Bench template for design  :  mupix4_emulator
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY mupix4_emulator_vhd_tst IS
END mupix4_emulator_vhd_tst;
ARCHITECTURE mupix4_emulator_arch OF mupix4_emulator_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL b : STD_LOGIC_VECTOR(24 DOWNTO 0);
SIGNAL clkin_50 : STD_LOGIC;
SIGNAL lcd_csn : STD_LOGIC;
SIGNAL lcd_d_cn : STD_LOGIC;
SIGNAL lcd_data : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL lcd_wen : STD_LOGIC;
SIGNAL user_dipsw : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL user_led_g : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL user_led_r : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL user_pb : STD_LOGIC_VECTOR(2 DOWNTO 0);
COMPONENT mupix4_emulator
	PORT (
	b : INOUT STD_LOGIC_VECTOR(24 DOWNTO 0);
	clkin_50 : IN STD_LOGIC;
	lcd_csn : OUT STD_LOGIC;
	lcd_d_cn : OUT STD_LOGIC;
	lcd_data : INOUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	lcd_wen : OUT STD_LOGIC;
	user_dipsw : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	user_led_g : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	user_led_r : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	user_pb : IN STD_LOGIC_VECTOR(2 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : mupix4_emulator
	PORT MAP (
-- list connections between master ports and signals
	b => b,
	clkin_50 => clkin_50,
	lcd_csn => lcd_csn,
	lcd_d_cn => lcd_d_cn,
	lcd_data => lcd_data,
	lcd_wen => lcd_wen,
	user_dipsw => user_dipsw,
	user_led_g => user_led_g,
	user_led_r => user_led_r,
	user_pb => user_pb
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          
END mupix4_emulator_arch;
