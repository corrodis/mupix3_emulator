-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "12/03/2013 11:06:24"
                                                            
-- Vhdl Test Bench template for design  :  sm_mupix
-- 
-- Simulation tool : ModelSim-Altera (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;          
use IEEE.numeric_std.all;                  

ENTITY sm_mupix_vhd_tst IS
END sm_mupix_vhd_tst;
ARCHITECTURE sm_mupix_arch OF sm_mupix_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL BUSY : STD_LOGIC;
SIGNAL CLK : STD_LOGIC;
SIGNAL COL : STD_LOGIC_VECTOR(5 DOWNTO 0);
SIGNAL DATA_INV : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL EVENTS_NO : INTEGER;
SIGNAL LDCOL : STD_LOGIC;
SIGNAL LDPIX : STD_LOGIC;
SIGNAL PRIOUT : STD_LOGIC;
SIGNAL PULLDOWN : STD_LOGIC;
SIGNAL RDCOL : STD_LOGIC;
SIGNAL READOUT_NO : INTEGER;
SIGNAL ROW : STD_LOGIC_VECTOR(5 DOWNTO 0);
SIGNAL RST : STD_LOGIC;
SIGNAL SET_PIX_NO : STD_LOGIC;
SIGNAL STAT_STATE : STD_LOGIC_VECTOR(3 DOWNTO 0);
SIGNAL TSTAMP : STD_LOGIC_VECTOR(7 DOWNTO 0);
COMPONENT sm_mupix
	PORT (
	BUSY : OUT STD_LOGIC;
	CLK : IN STD_LOGIC;
	COL : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
	DATA_INV : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	EVENTS_NO : OUT INTEGER;
	LDCOL : IN STD_LOGIC;
	LDPIX : IN STD_LOGIC;
	PRIOUT : OUT STD_LOGIC;
	PULLDOWN : IN STD_LOGIC;
	RDCOL : IN STD_LOGIC;
	READOUT_NO : OUT INTEGER;
	ROW : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
	RST : IN STD_LOGIC;
	SET_PIX_NO : IN STD_LOGIC;
	STAT_STATE : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
	TSTAMP : IN STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
END COMPONENT;
BEGIN
	i1 : sm_mupix
	PORT MAP (
-- list connections between master ports and signals
	BUSY => BUSY,
	CLK => CLK,
	COL => COL,
	DATA_INV => DATA_INV,
	EVENTS_NO => EVENTS_NO,
	LDCOL => LDCOL,
	LDPIX => LDPIX,
	PRIOUT => PRIOUT,
	PULLDOWN => PULLDOWN,
	RDCOL => RDCOL,
	READOUT_NO => READOUT_NO,
	ROW => ROW,
	RST => RST,
	SET_PIX_NO => SET_PIX_NO,
	STAT_STATE => STAT_STATE,
	TSTAMP => TSTAMP
	);
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;  
                                         
clock : PROCESS  
BEGIN  
  CLK <= '0';
  WAIT for 2 ns;
  CLK <= not CLK;     
  WAIT for 2 ns;                                                                                   
END PROCESS clock;

test_pattern : PROCESS 

BEGIN     
  -- resets
  RST <= '0';
  LDCOL <= '0';
  RDCOL <= '0';
  LDPIX <= '0';
  SET_PIX_NO <= '1';
  
  
  -- input data
  ROW <= "101010";
  COL <= "010101";
  TSTAMP <= "11001100";
  
  WAIT for 50 ns;
  -- lets start
  RST <= '1';
  WAIT for 5 ns;
  LDPIX <= '1';
  WAIT for 5 ns;
  LDPIX <= '0';
  
  -- load col
   WAIT for 10 ns;
   LDCOL <= '1';
   WAIT for 10 ns;
   LDCOL <= '0';
   
   -- read 
   for I in 1 to 70 loop
    WAIT for 10 ns;
    RDCOL <= not RDCOL;
    ROW <= STD_LOGIC_VECTOR(unsigned(ROW)+1);
    if I mod  15=0 then
      LDCOL <= '1';
    else
      LDCOL <= '0';
    end if;
end loop;
                
    WAIT;      
END PROCESS test_pattern;                                           
END sm_mupix_arch;
