transcript on
if ![file isdirectory template_iputf_libs] {
	file mkdir template_iputf_libs
}

if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

###### Libraries for IPUTF cores 
###### End libraries for IPUTF cores 
###### MIF file copy and HDL compilation commands for IPUTF cores 


vcom "C:/Daten/mupix3_emulation/PLL_ts_sim/PLL_ts.vho"

vcom -93 -work work {C:/Daten/mupix3_emulation/controller_v2.vhd}
vcom -93 -work work {C:/Daten/mupix3_emulation/gray_counter.vhd}
vcom -93 -work work {C:/Daten/mupix3_emulation/rnGen.vhd}
vcom -93 -work work {C:/Daten/mupix3_emulation/mupix4_emulator.vhd}
vcom -93 -work work {C:/Daten/mupix3_emulation/sm_mupix.vhd}

