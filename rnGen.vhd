-- Generates 8bit rn for cols and rows
-- Simon Corrodi, corrodi@physi.uni-heidelberg.de, 8/11/2013

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity rnGen is
	Port ( clk 			: in	std_logic;
			 rst			: in  std_logic;
			 en			: in  std_logic;
			 seed			: in  std_logic_vector(15 downto 0) := x"AABB";
			 rn			: out std_logic_vector(15 downto 0));
end rnGen;

architecture ar_dataGen of rnGen is

	signal rn_int : std_logic_vector(15 downto 0);

begin

	rnGeneration: process(clk) begin
		if (rst = '1') then
			rn_int <= seed;
		elsif (rising_edge(clk)) then
			if (en = '1') then
				rn_int <= rn_int(14 downto 0) & (rn_int(15) xor rn_int(13) xor rn_int(12) xor rn_int(10));
			else
				rn_int <= rn_int;
			end if;
		end if;
	end process rnGeneration;

	rn <= rn_int;
	
end ar_dataGen;			 