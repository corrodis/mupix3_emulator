-- Manages what is send when in which order
-- 12/11/13 Simon: added ppossabilitz to add some randomness to the tstamp, set set_tstamp_rn

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;

entity outputManager is
	port(
		clk 	 			: in  std_logic; 							-- same clk as 8b/10b (clk_words))
		rst 	 			: in  std_logic;
		row 	 			: in  std_logic_vector(7 downto 0);
		col 	 			: in  std_logic_vector(7 downto 0);
		tstamp 			: in  std_logic_vector(7 downto 0);
		user_set_datak : in  std_logic;
		set_rn		 	: in  std_logic;							-- only send sometimes hits
		set_tstamp_rn 	: in  std_logic;							-- add some variation into tstamp
		data				: out std_logic_vector(7 downto 0);
		set_datak 		: out std_logic;
		send_event		: out std_logic);
end entity outputManager;

architecture ar_outMan of outputManager is

	component rnGen 
		Port (
			clk 				: in	std_logic;
			rst				: in  std_logic;
			en					: in  std_logic;
			rn					: out std_logic_vector(7 downto 0));
	end component;
	
	
	signal event_buffer   : std_logic_vector(15 downto 0);
	signal rn_data			 : std_logic_vector(7 downto 0);
	signal cnt_event_part : integer := 0;
	signal tstamp_mod		 : std_logic_vector(7 downto 0);
	signal tstamps	 		 : std_logic_vector(63 downto 0);			-- used to add some randomness in tstamp, save 8 tstamps
	signal cnt_tstamps 	 : integer := 0;									-- counter used to save tstamps
	
	-- debug
	signal cnt_deb : integer := 0;
	
begin

rn : rnGen port map(
		clk 				=> clk,
		rst				=> rst,
		en					=> set_rn,
		rn					=> rn_data);
		
	manager : process(clk) begin
		if(rising_edge(clk)) then
			if(rst = '1' or user_set_datak = '1') then
				event_buffer 	<= (others=>'0');
				data				<= x"3C";										-- used for sync
				set_datak 		<= '1';
				send_event		<= '0';
				tstamp_mod		<= tstamp;
				tstamps			<= tstamp & tstamp & tstamp & tstamp & tstamp & tstamp & tstamp & tstamp;
				cnt_tstamps		<= 0;
			else
				set_datak <= '0';
				-- debug, counter to send a couple of times the same rn
--				if(cnt_deb>800000000) then
--					data <= rn_data;
--					cnt_deb <= 0;
--				else
--					cnt_deb <= cnt_deb + 1;
--				end if;
				if(cnt_event_part > 0) then 
					-- manager is currently sending an event, after tstamp has already been sent, send now col and row
					send_event 	<= '1';
					set_datak 	<= '0';
					-- changes tstamp to be not perfectly aligned, only if set_tstamp_rn is on (randomly pcik one of the last 8th greycodes)
					if(set_tstamp_rn = '1') then
						case cnt_tstamps is			-- save last 8 tstamps
								when 0 => tstamps( 7 downto  0) <= tstamp;
								when 1 => tstamps(15 downto  8) <= tstamp;
								when 2 => tstamps(23 downto 16) <= tstamp;
								when 3 => tstamps(31 downto 24) <= tstamp;
								when 4 => tstamps(39 downto 32) <= tstamp;
								when 5 => tstamps(47 downto 40) <= tstamp;
								when 6 => tstamps(55 downto 48) <= tstamp;
								when 7 => tstamps(63 downto 56) <= tstamp;
								when others => tstamps( 7 downto  0) <= tstamp;
							end case;
					
						if(cnt_tstamps > 7) then -- count to 7
							cnt_tstamps <= 0;
						else
							cnt_tstamps <= cnt_tstamps + 1;
						end if;
						
						case rn_data(6 downto 4) is			-- pick randomly one of this tstamps
								when "000" => tstamp_mod <= tstamps( 7 downto  0);
								when "001" => tstamp_mod <= tstamps(15 downto  8);
								when "010" => tstamp_mod <= tstamps(23 downto 16);
								when "011" => tstamp_mod <= tstamps(31 downto 24);
								when "100" => tstamp_mod <= tstamps(39 downto 32);
								when "101" => tstamp_mod <= tstamps(47 downto 40);
								when "110" => tstamp_mod <= tstamps(55 downto 48);
								when "111" => tstamp_mod <= tstamps(63 downto 56);
								when others => tstamp_mod <= tstamp;
							end case;
					else
						tstamp_mod <= tstamp;
					end if;
					case cnt_event_part is
						when 1 =>  data <= event_buffer(15 downto 8);
										 cnt_event_part <= cnt_event_part + 1;					
						when 2 =>  data <= event_buffer( 7 downto 0);
										 cnt_event_part <= 0;
						when others =>
										 cnt_event_part <= 0;
					end case;
				else
					-- currently not sending an event
					if(event_buffer /= col & row and (rn_data(0) = '0' or set_rn = '0')) then
						send_event 		<= '1';
						set_datak 		<= '0';
						data 				<= tstamp_mod;
						event_buffer 	<= col & row;
						cnt_event_part <= cnt_event_part + 1;
					else
						send_event 		<= '0';
						set_datak	 	<= '1';
						data 				<= x"BC";
					end if;
				end if;
				
				
			
			
			end if;
		end if;
	end process;
end architecture ar_outMan;